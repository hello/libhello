# libhello

A simple library that implements the "Hello World" example in C++. Its primary
goal is to show a canonical `build2`/`bpkg` project/package.
