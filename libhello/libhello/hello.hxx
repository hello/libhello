#pragma once

#include <iosfwd>
#include <string>

#include <libformat/format.hxx>

#include <libhello/export.hxx>

namespace hello
{
  // If you compare this interface to version 1.0, then you will notice that
  // while it is API/source-compatible (the call via the old signature of
  // say_hello() is still valid) it is not ABI/binary-compatible (say_hello()
  // now has an extra argument and is inline).
  //
  // Notice also that say_hello() now uses a type and calls (from its inline
  // implementation) a function from format which means libformat is an
  // "interface dependency" of libhello.

  LIBHELLO_SYMEXPORT void
  say_hello_formatted (std::ostream&, const std::string& hello);

  inline void
  say_hello (std::ostream& o,
             const std::string& name,
             format::volume v = format::volume::normal)
  {
    say_hello_formatted (o, format::format_hello ("Hello", name, v));
  }
}
