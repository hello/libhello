#include <libhello/hello.hxx>

#include <libprint/print.hxx>

using namespace std;

namespace hello
{
  void
  say_hello_formatted (ostream& o, const string& h)
  {
    print::print_hello (o, h);
  }
}
