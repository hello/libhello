#include <sstream>

#include <libhello/version.hxx>
#include <libhello/hello.hxx>

#undef NDEBUG
#include <cassert>

int main ()
{
  using namespace std;
  using namespace hello;

  // Basics.
  //
  {
    ostringstream o;
    say_hello_formatted (o, "Hi, World!");
    assert (o.str () == "Hi, World!\n");
  }

  {
    ostringstream o;
    say_hello (o, "World");
    assert (o.str () == "Hello, World!\n");
  }

  // Volume.
  //
  {
    ostringstream o;
    say_hello (o, "World", format::volume::loud);
    assert (o.str () == "HELLO, World!\n");
  }
}
