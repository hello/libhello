// file: libmhello/hello.cxx -*- C++ -*-

module hello;

#ifdef __clang__
import std.core;
#endif

import print;

using namespace std;

namespace hello
{
  void
  say_formatted (const string& m)
  {
    print::to_stdout (m);
  }
}
