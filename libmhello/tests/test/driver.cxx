// file: tests/test/driver.cxx -*- C++ -*-

import std.core;
import format;   // volume
import hello;

int
main ()
{
  using namespace hello;

  say ("World");
  say ("World", format::volume::loud);
  say_formatted ("Hi, World!");
}
